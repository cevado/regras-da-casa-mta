* Setting
A história irá se passar em Istambul no meio do ano de 2018.
De contexto para os jogadores, Istambul tem um grande fluxo de mercadorias e um forte mercado negro.
Este tipo de comercio se tornou mais forte após a recessão econômica e o distanciamento da União Europeia.
Em 2018 a seita majoritária são os batini, seguido pelos taftani e os euthanatoi(ainda usam esse nome, por uma maioria ser de gregos).
Existe uma concentração de bata'a, ngoma e akashayana devido a uma presença de etíopes e japoneses.
Apesar disso existe um grande fluxo de despertos que viajam pra cidade para comercializar antiguidades que podem ser usadas como foco.
Deste fluxo uma quantidade majoritária são Herméticos, mas despertos de qualquer seita viajam pra lá em busca dessas antiguidades.

** Timeline adaptada com pontos de vista dos despertos
+ Durante o Império Otomano não se tem muita informação sobre despertos na região, especula-se que a maioria eram de batinis, taftanis e euthanatoi.
+ Entre os dispertos sabe-se que as reformas ataturcas e o Kemalismo foi uma grande investida tecnocrata pra se estabelecer na região e tentar trazer o oriente médio pro progrom.(~1922)
+ Rebeliões contra reformas ataturcas apesar de não serem iniciadas por despertos, teve bastante envolvimento de todas as facções da região.(1925~1938)
+ Os batini dizem que o Golpe Mordad 28 no Irã é uma expansão desse projeto tecnocrata.(1953)
+ Crise do Chipre e Invasão do Chipre pela Turquia, conflito entre euthanatoi e batini acontecem nesse processo apesar de ter inicio por questões politicas locais.(1963~1964,1974)
+ Os euthanatoi dizem que o início do conflito curdo-turco é na realidade um sinal de aliança dos batini com os tecnocratas.(1978)
+ Os batini contra argumentam essa acusação dizendo que apoiam o povo curdo e que é tudo parte da estratégia da Tecnocracia pra uniformizar a região.
+ Levante ultranacionalista turco, despertos acusam de ser ação direta da tecnocracia pra suprimir as comunidades despertas.(1976-1980)
+ Os Euthanatoi dizem que a Revolução Iraniana é responsabilidade direta de ação dos Batini em conflito com os Tecnocratas.(1979)
+ Guerra Civil Iraquiana-Curda, batini e taftani turcos acusam a situação como resultado da expansão da Tecnocracia na região.(1997)
+ Aproximação da União Européia, tentativa de entrada pra UE. Fortes atividades da tecnocracia pra supressão de transgressores da realidade.(2001~2014)
+ Guerra Civil Síria, envolvimento do Daesh, piora a questão Curda. Percebe-se um aumento da atividade Nefandi na região e uma queda da atividade da Tecnocracia.(2011-presente)
+ Erdogan presidente, nova constituição, afastamento da UE. Associam essas questões a uma redução de atividades da Tecnocracia na região.(2014-presente)
+ Tentativa de golpe/Auto-golpe em Ancara/Istambul. Batini dizem que foi uma tentativa da Tecnocracia de voltar a ter relevância na região. Euthanatoi acusam Batinis de ajudar o Governo Turco num auto-golpe pra instituir um Sultanato na Turquia.(2016)
+ Mercado Negro em alta, entre despertos corre o boato de sorvos e inclusive maravilhas sendo vendidas indiscriminadamente inclusive para mortais.(2018)


** Mercado Negro
O mercado negro em Istambul pode ser dividido em duas camadas.
O contrabando normal em Istambul, de armas, drogas(licitas e ilicitas, principalmente de farmacos proibidos na europa) e exoticidades(animais de caça proibida internacionalmente, produtos de processo proibido, etc.), jogadores podem ter acesso com Contatos e Aliados e até Influência nesse tipo de mercado.
Inclusive com alguns testes de subterfugio e streetwise.
Existe também um mercado fechado pra despertos.
Para esse mercado fechado, apenas com Contatos ou Aliados para ter acesso.
É necessário Contatos 3(Focado em comunidade desperta) ou Aliados 1(focado nisso).
Streetwise e Subterfugio puros não vão permitir acesso.
Jogadores que desejarem ter acesso ao mercado fechado pra despertos por favor informem com antecedencia.


** Repressão
O governo turco tem manifestado uma escalada autoritária.
Um afastamento do secularismo caraterístico do país nos ultimos anos, e cada vez mais se direcionado pra uma possível teocracia islamica.
Após a tentativa de golpe de 2016, o governo tem dificuldado qualquer prática religiosa que não seja o islamismo suunita.
Prisão de professores e fechamento de Universidades.
Prisão e assassinatos de jornalistas.
Extradição de jornalistas estrangeiros.
Censura e bloqueio de acesso a a internet e casos de total bloqueio de acesso a internet, principalmente durante protestos.
